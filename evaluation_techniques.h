#pragma once
#include "DistCalculator.h"
#include "naivenn_base.h"
#include "point.h"
#include <algorithm>
#include <iostream>
#include <limits>
#include <stdexcept>
#include <vector>
using namespace std;

double proportionOfNoises(vector<Point>* c) {
  int n = 0;
  for(Point& p : *c)
    if(p.getClusterNumber()==-1)
      n++;

  return n/(double)c->size();
}

double groupSilhouetteMeasure(vector<Point>* c) {
  double res = 0;
  int n = c->size();
  double pseudocount = 1e-15;

  for(int i=0; i<n; i++) {
    if(c->at(i).getClusterNumber()!=-1) {
      vector<double> dists1;
      for(int j=0; j<n; j++)
        if(i!=j && c->at(i).getClusterNumber()==c->at(j).getClusterNumber())
          dists1.push_back(calculateMinkowskiDistance(&c->at(i), &c->at(j)));
      double ax = (dists1.size()>0 ? accumulate(dists1.begin(), dists1.end(), 0)/(double)dists1.size() : 0) + pseudocount;

      int maxClustID = -1;
      for(Point& p : *c)
        if(maxClustID<p.getClusterNumber())
          maxClustID = p.getClusterNumber();

      vector<vector<double> > dists2(maxClustID+1, vector<double>());
      for(int j=0; j<n; j++)
        if(i!=j && c->at(i).getClusterNumber()!=c->at(j).getClusterNumber() && c->at(j).getClusterNumber()!=-1)
          dists2.at(c->at(j).getClusterNumber()).push_back(calculateMinkowskiDistance(&c->at(i), &c->at(j)));
      double bx = numeric_limits<double>::max();
      for(int j=0; j<dists2.size(); j++)
        if(c->at(i).getClusterNumber()!=j) {
          double avg_dist = (dists2.at(j).size()>0 ? accumulate(dists2.at(j).begin(), dists2.at(j).end(), 0)/(double)dists2.at(j).size() : 0) + pseudocount;
          if(avg_dist < bx)
            bx = avg_dist;
        }

      res += (bx-ax)/(double)max(bx, ax);
    }
  }

  return res/(double)n;
}

double randIndex(vector<Point>* c1, vector<Point>* c2) {
  int n = c1->size();
  if(n!=c2->size())
    throw runtime_error("ERROR: The two data structures had different numbers of elements!!!!!");

  int A = 0, B = 0;
  for(int i=0; i<n; i++)
    for(int j=i+1; j<n; j++)
      if(c1->at(i).getClusterNumber()==c1->at(j).getClusterNumber() && c2->at(i).getClusterNumber()==c2->at(j).getClusterNumber())
        A++;
      else if(c1->at(i).getClusterNumber()!=c1->at(j).getClusterNumber() && c2->at(i).getClusterNumber()!=c2->at(j).getClusterNumber())
        B++;

  return (A+B)/(double)(n*(n-1)/2);
}

/*
 * Returns all generated clusters with number of points assigned to them.
 * Cluster -1 is a noise. Cluster 0 and above are genuine.
 * author: Łukasz Patro
 */
map<int, int> cardinalities(vector<Point>* dataset) {
  map<int, int> clusters;

  for (int i = 0; i < dataset->size(); i++) {
    int ithPointClusterNumber = dataset->at(i).getClusterNumber();
    auto searchedCluster = clusters.find(ithPointClusterNumber);
    if (searchedCluster == clusters.end()) {
      clusters.insert(pair<int,int>(ithPointClusterNumber, 1));
    } else {
      searchedCluster->second++;
    }
  }

  return clusters;
}

tuple<int, int, int, double> clustStats(vector<Point>* c) {
  vector<int> stats;
  for(auto& card : cardinalities(c))
    stats.push_back(card.second);

  int maxNum = *max_element(stats.begin(), stats.end());
  int minNum = *min_element(stats.begin(), stats.end());
  double mean = accumulate(stats.begin(), stats.end(), 0)/stats.size();

  double variance = 0;
  for(int& m : stats)
    variance += (m-mean)*(m-mean);
  variance /= stats.size()-1;

  return make_tuple(stats.size(), maxNum, minNum, sqrt(variance));
}
