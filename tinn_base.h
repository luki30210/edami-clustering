//
// Created by Rafał Pytel on 16.05.2018.
//
#pragma once

#include "point.h"
#include "distance_point.h"
#include "utils.h"
#include "DistCalculator.h"


#ifndef DOWNLOAD_TINN_H
#define DOWNLOAD_TINN_H

#endif //DOWNLOAD_TINN_H
using namespace std::chrono;
/**
 * Class that helps organizing K Nearest Nieghbours for certain point
 */
class KNN {
public:
    KNN(int k) {
        distancePoints = vector<DistancePoint *>();
        this->k = k;
    }

    void addDistancePoint(DistancePoint *distancePoint) {
        distancePoints.push_back(distancePoint);
        if (distancePoints.size() > k) {
            sort(distancePoints.begin(), distancePoints.end(), compDistancePoints);
            int tempK = k - 1;
            while (distancePoints.size() > (tempK + 1) && distancePoints.at(tempK) ==
                                                          distancePoints.at(tempK + 1)) {
                tempK++;
            }

            distancePoints.erase(distancePoints.begin() + tempK + 1, distancePoints.end());

            currEps = distancePoints.at(tempK)->distance;

        }
    }

    const vector<DistancePoint *> &getDistancePoints() const {
        return distancePoints;
    }

    double getCurrEps() const {
        return currEps;
    }

private:
    vector<DistancePoint *> distancePoints;
    double currEps = 100000;
    int k;

};
/**
 * Function to compute reference Point for (0,0...0)
 * @param point
 * @return
 */
Point *createReferencePoint(Point *point) {
    vector<double> attributes(point->getData()->size(), 0.0);
    return new Point(&attributes);

}
/**
 * Function to compute reference Point for custom attributes (can be max or min of dataset)
 * @param attributes
 * @return
 */
Point *createReferenceCustomAttrPoint(vector<double> attributes){
    return new Point(&attributes);

}

/**
 * Function to calculate reference distances for dataset
 * @param dataset
 * @return
 */
static vector<DistancePoint *> calculateReferenceDistances(vector<Point> *dataset, DistCalculator d) {
    vector<DistancePoint *> distances = vector<DistancePoint *>();
    Point* referencePoint = createReferencePoint(&dataset->at(1));

    for (int i = 0; i < dataset->size(); i++) {
        Point *point = &dataset->at(i);
        DistancePoint *distancePoint = new DistancePoint(point,
                                                         d(point, referencePoint));
        distances.push_back(distancePoint);
    }

    sort(distances.begin(), distances.end(), compDistancePoints);
    return distances;
}
/**
 * Function that check if current Point is in eps Neighberhood of chosen Point
 * @param p
 * @param rp
 * @param k
 * @param sortedDataset
 * @param referenceDistances
 * @param currKnn
 * @return
 */
static bool
searchIfBetter(int p, int rp, int k, vector<Point *> *sortedDataset, vector<DistancePoint *> *referenceDistances,
               KNN *currKnn, DistCalculator d) {
    if (currKnn->getDistancePoints().size() >= k) {
        DistancePoint *referenceDistanceRP = referenceDistances->at(rp);
        double proposedDistance = abs(referenceDistances->at(p)->distance - referenceDistanceRP->distance);

        //CHECK TRIANGLE INEQUITY
        if (proposedDistance <= currKnn->getCurrEps()) {
            double realDistance = abs(d(sortedDataset->at(p), sortedDataset->at(rp)));

            //CHECK CURRENT
            if (realDistance <= currKnn->getCurrEps()) {
                DistancePoint *distancePoint = new DistancePoint(sortedDataset->at(rp),
                                                                 realDistance);
                currKnn->addDistancePoint(distancePoint);
            }
        } else {
            return false;
        }
    } else {
        //compute first k NN
        DistancePoint *distancePoint = new DistancePoint(sortedDataset->at(rp),
                                                         d(sortedDataset->at(p), sortedDataset->at(rp)));
        currKnn->addDistancePoint(distancePoint);
    }
    return true;
}
/**
 * Function for sorting dataset according to reference Point(0,0...0)
 * @param dataset
 * @return
 */
vector<Point*> getDataSetSorted(vector<Point> *dataset, DistCalculator d){
    vector<DistancePoint *> referenceDistances = calculateReferenceDistances(dataset, d);
    vector<Point *> *sortedDataset =new vector<Point *>();

    for (int i = 0; i < referenceDistances.size(); i++) {
        sortedDataset->push_back(referenceDistances.at(i)->refPoint);
    }
    return *sortedDataset;
}
/**
 * Function that calculates nearest neighbours for dataset
 * @param dataset
 * @param k
 * @return times for processing and execution
 */
static vector<double> tinn(vector<Point> *dataset, int k, DistCalculator d) {
    vector<double> times = vector<double>();
    k--;

    ///////PREPROCESSING//////////
    high_resolution_clock::time_point t1_preprocessing = high_resolution_clock::now();
    //Calculating reference distance to 0,0..
    vector<DistancePoint *> referenceDistances = calculateReferenceDistances(dataset, d);
    vector<Point *> sortedDataset = vector<Point *>();
    map<Point *, KNN *> knnMap = map<Point *, KNN *>();

    for (int i = 0; i < referenceDistances.size(); i++) {
        sortedDataset.push_back(referenceDistances.at(i)->refPoint);
    }

    high_resolution_clock::time_point t2_preprocessing = high_resolution_clock::now();
    times.push_back(((double) duration_cast<milliseconds>(t2_preprocessing - t1_preprocessing).count()) / 1000);


    //////EXECUTION///////////
    high_resolution_clock::time_point t1_execution = high_resolution_clock::now();
    for (int p = 0; p < sortedDataset.size(); p++) {
        KNN *currKnn = new KNN(k);
        DistancePoint *referenceDistanceP = referenceDistances.at(p);

        //FORWARD SEARCH
        bool end = true;
        int rp = p + 1;
        while (end && rp < sortedDataset.size()) {
            end = searchIfBetter(p, rp, k, &sortedDataset, &referenceDistances, currKnn, d);
            rp = rp + 1;
        }

        //BACKWARD SEARCH
        end = true;
        rp = p - 1;
        while (end && rp >= 0) {
            end = searchIfBetter(p, rp, k, &sortedDataset, &referenceDistances, currKnn, d);
            rp = rp - 1;
        }
        knnMap[referenceDistanceP->refPoint] = currKnn;
    }

    //REWRITE DATASET
    for (int i = 0; i < dataset->size(); i++) {
        Point *point = &dataset->at(i);
        vector<DistancePoint *> distancePoints = knnMap[point]->getDistancePoints();
        for (DistancePoint *dp : distancePoints) {
            point->addKpNN(dp->refPoint);
            dp->refPoint->incrementRkpNNCount();
        }
    }
    high_resolution_clock::time_point t2_execution = high_resolution_clock::now();

    times.push_back(((double) duration_cast<milliseconds>(t2_execution - t1_execution).count()) / 1000);
    return times;

}
