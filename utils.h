#pragma once
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <map>
#include <initializer_list>
#include "point.h"
#include "evaluation_techniques.h"

using namespace std;

/**
* This method reads contents from a text/data file where each row consists of numerical values that are separated from each other by some string delimiter.
* A user has the possibility to remove unwanted colummns, select only a certain number of rows from the provided file and decide whether the selection of
* data rows should be randomized or not (under the condition that it is decided that only a subset of the data is about to be retrieved).
*/
vector<Point> readPointsFromFile(string URL, string delimiter, vector<int> excludeCols, int attribute, bool labelled) {
  vector<Point> res;

  ifstream contents;
  contents.open(URL);
  if(!contents) {
    cerr<<"ERROR! The file with name " + URL + " could not be found!"<<endl;
    exit(1);
  }

  string line;
  while(getline(contents, line)) {
    vector<double> dataPoint;

    int colId = 1;
    size_t previous = 0, current = line.find(delimiter);
    bool once = true;
    int groupId = -1;
    while(current != string::npos) {
      string token = line.substr(previous, current - previous);

      if(find(excludeCols.begin(), excludeCols.end(), colId) == excludeCols.end() && colId!=attribute)
        dataPoint.push_back(atof(token.c_str()));
      else if(labelled && colId==attribute)
        groupId = atof(token.c_str());

      colId++;
      previous = current + 1;
      current = line.find(delimiter, previous);
      if(once && current == string::npos) {
        current = line.length();
        once = false;
      }
    }

    Point* p = new Point(&dataPoint);
    p->setClusterNumber(groupId);
    res.push_back(*p);
  }

  contents.close();
  return res;
}

size_t storeIntoFile(vector<pair<vector<pair<int,double> >, vector<int> > > KNNandRKNN, string URL, int fixed_rowsize = 0) {
  ofstream file;
  file.open(URL);
  size_t maxNumChars = 0;

  for(int i=0; i<KNNandRKNN.size(); i++) {
    string s(to_string(i) + " (");
    if(fixed_rowsize > 0)
      s.reserve(fixed_rowsize);

    for(int j=0; j<KNNandRKNN.at(i).first.size(); j++) {
      pair<int, double> somePair = KNNandRKNN.at(i).first.at(j);

      if(j==KNNandRKNN.at(i).first.size()-1)
        s += to_string(somePair.first);
      else
        s += to_string(somePair.first) + ',';
    }
    s += ") (";

    for(int j=0; j<KNNandRKNN.at(i).second.size(); j++) {
      int someInt = KNNandRKNN.at(i).second.at(j);

      if(j==KNNandRKNN.at(i).second.size()-1)
        s += to_string(someInt);
      else
        s += to_string(someInt) + ',';
    }
    s += ')';

    file << s << endl;

    if(s.length() > maxNumChars)
      maxNumChars = s.length();
  }

  file.close();
  return maxNumChars;
}
