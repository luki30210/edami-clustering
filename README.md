# NBC clustering

## Params:

`-i`	inputFile

`-c`	classColumn

`-e`	excludedColumns

`-k`	kNearestNeighbours

`-s`	separator (default: `,`)

`-o`	optimization (TI)

`-d`	distanceMetricNumber (`1` - Minkowsky, `0` - cosine similarity; default: `1`)

`-p`	minkowskyParameter

`-t`	normalizedVectors (cosine similarity)

`-a`	alfaValue (default: `1`)

`-f`	outputFile


## Example runs:

./build/main -i datasets/CNAE-9.data -c 1 -f outFiles/3.2.1_cnae.txt

./build/main -i datasets/epileptic.data -e 1 -c 179 -f outFiles/3.2.1_epi.txt

./build/main -i datasets/movement_libras.data -c 91 -f outFiles/3.2.1_mov.txt

./build/main -i datasets/CNAE-9.data -c 1 -d 0 -f outFiles/3.2.2_cnae.txt

./build/main -i datasets/epileptic.data -e 1 -c 179 -d 0 -f outFiles/3.2.2_epi.txt

./build/main -i datasets/movement_libras.data -c 91 -d 0 -f outFiles/3.2.2_mov.txt

./build/main -i datasets/CNAE-9.data -c 1 -d 0 -t -f outFiles/3.2.3_cnae.txt

./build/main -i datasets/epileptic.data -e 1 -c 179 -d 0 -t -f outFiles/3.2.3_epi.txt

./build/main -i datasets/movement_libras.data -c 91 -d 0 -t -f outFiles/3.2.2_mov.txt

./build/main -i datasets/CNAE-9.data -c 1 -o -f outFiles/3.3.1_cnae.txt

./build/main -i datasets/epileptic.data -e 1 -c 179 -o -f outFiles/3.3.1_epi.txt

./build/main -i datasets/movement_libras.data -c 91 -o -f outFiles/3.3.1_mov.txt

./build/main -i datasets/CNAE-9.data -c 1 -d 0 -o -f outFiles/3.3.2_cnae.txt

./build/main -i datasets/epileptic.data -e 1 -c 179 -d 0 -o -f outFiles/3.3.2_epi.txt

./build/main -i datasets/movement_libras.data -c 91 -d 0 -o -f outFiles/3.3.2_mov.txt

./build/main -i datasets/CNAE-9.data -c 1 -d 0 -o -t -f outFiles/3.3.3_cnae.txt

./build/main -i datasets/epileptic.data -e 1 -c 179 -d 0 -o -t -f outFiles/3.3.3_epi.txt

./build/main -i datasets/movement_libras.data -c 91 -d 0 -o -t -f outFiles/3.3.3_mov.txt
