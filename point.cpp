#include "point.h"

Point::Point(vector<double> *data) {
  this->data = *data;
}

int Point::getClusterNumber() {
  return this->clusterNumber;
}

void Point::setClusterNumber(int clusterNumber) {
  this->clusterNumber = clusterNumber;
}

double Point::getNdf() {
  return this->ndf;
}

void Point::setNdf(double ndf) {
  this->ndf = ndf;
}

vector<Point *> Point::getKpNN() {
  return this->kpNN;
}

void Point::addKpNN(Point *point) {
  this->kpNN.push_back(point);
}

unsigned int Point::getRkpNNCount() {
  return this->rkpNNCount;
}

void Point::incrementRkpNNCount() {
  (this->rkpNNCount)++;
}

vector<double> *Point::getData() {
  return &this->data;
}
