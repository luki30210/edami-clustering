#include <iostream>
#include <chrono>
#include "utils.h"
#include "naivenn_base.h"
#include "tinn_base.h"
#include "evaluation_techniques.h"
#include "nbc.h"

using namespace std;
using namespace std::chrono;

int main(int argc, char **argv) {
  // INITIALIZE PARAMETERS
  string name; // Filename
  int c_attr = 1; // Class attribute of dataset
  int k = 5; // Number of neighbors to apply
  string sep = ",";
  vector<int> exclude; // Columns to ignore
  bool opt = false; // Indicate whether optimization of computations shall be performed
  int dist_metr = 1; // (1) Minkowski distance, (0) Cosine similarity
  int p = 2; // Parameter to the Minkowski distance
  bool sortAsTi = false;
  bool transform_cosine = false; // Transform calculation of cosine similarity into corresponding
                                 // calculation of Euclidean distance
  double alpha = 1;
  string out_file = "";
  bool headers = false;

  // LOAD PARAMETERS FROM ARGUMENTS
  for (int i = 1; i < argc; i++) {
    string parm = argv[i];

    // WITHOUT ADDITIONAL PARAMETERS
    if (parm.compare("-o") == 0) {
      /* Tell the program whether to make use of optimizations (using TI property) or not */
      opt = true;
    }

    else if(parm.compare("-t") == 0) {
      /* Tell the program, given that cosine similarity is used as distance metric, whether it
         should transform problem of calculating cosine similarities into correspondingly
         calculate the distances (using normalized vectors). */
      transform_cosine = true;
    }

    else if (parm.compare("--sort") == 0) {
      sortAsTi = true;
    }
    else if (parm.compare("-h") == 0) {
      headers = true;
    }


    // WITH ADDIDIONAL PARAMETERS
    else if (++i < argc) {

      if (parm.compare("-i") == 0) {
        /* Input file */
        name = argv[i];
      }

      else if (parm.compare("-c") == 0) {
        /* Tell the proram which column in the specified dataset is the class/decision (i.e. the grouping ID) */
        try {
          c_attr = stoi(argv[i]);
        }
        catch(...) {
          cerr<<"ERROR: The class column of the dataset must be a positive integer!"<<endl;
          return -1;
        }
      }

      else if (parm.compare("-e") == 0) {
        /* Tell the program which columns to remove from the dataset to be read. If not
          specified, then no columns will be removed.*/
        while (i < argc) {
          parm = argv[i];

          if (parm.find('-') != string::npos) {
            i--;

            if (exclude.size() == 0) {
              cerr << "ERROR: No column specified to exclude!" << endl;
              return -1;
            }

            break;
          }

          try {
            int tempIntParam = stoi(parm);
            if (tempIntParam < 0) {
              cerr << "ERROR: Each integer for column must be positive!" << endl;
              return -1;
            }
            exclude.push_back(tempIntParam);
          } catch(...) {
            cerr << "ERROR: Each column must be integer!" << endl;
            return -1;
          }

          i++;
        }
      }

      else if (parm.compare("-k") == 0) {
        try {
          k = stoi(argv[i]);
        } catch(...) {
          cerr << "ERROR: The value on k should be a integer!" << endl;
          return -1;
        }
        if (k < 0) {
          cerr << "ERROR: The value on k should be a positive integer!" << endl;
          return -1;
        }
      }

      else if(parm.compare("-s") == 0) {
        sep = argv[i];
      }

      else if(parm.compare("-d") == 0) {
      /* Tell the program which distance metric to make use of */
        try {
          dist_metr = stoi(argv[i]);
        } catch(...) {
          cerr<<"ERROR! The argument to identity of distance metric must be a integer!"<<endl;
          return -1;
        }
      }

      else if(parm.compare("-p") == 0 ) {
        /* Tell the program which value on p in the Minkg++ -std=c++11 point.cpp main.cpp -o mainowski distance to use (NOTE: Only works if
          the Minkowski distance was selected) */
        try {
          p = stoi(argv[i]);
        } catch(...) {
          cerr<<"ERROR! The argument to p of the Minkowski distance must be a positive integer!"<<endl;
          return -1;
        }
      }

      else if(parm.compare("-a") == 0) {
        /* Tell the program which value of alpha is supposed to be used in all calculations. If "-a" not
          specified, then alpha will be by default set to 1. */
        try{
          alpha = stoi(argv[i]);
        } catch(...) {
          cerr<<"ERROR! Alpha should be a non-zero numerical!"<<endl;
          return -1;
        }
      }

      else if(parm.compare("-f") == 0) {
        /* Tell the program in which file the output should be stored. If "-f" is not flagged, then
          the program will write all outputs in the terminal. */
        out_file = argv[i];
      }

    }

    else {
      cerr<<"ERROR: Could not recognize some of the flag(s)!"<<endl;
      return -1;
    }
  }

  // PREPARE DATA FOR NBC ALGORITHM
  high_resolution_clock::time_point pars1 = high_resolution_clock::now();
  vector<Point> dataset_labelled = readPointsFromFile(name, sep, exclude, c_attr, true);  // load labelled data from file
  vector<Point> dataset_unlabelled = readPointsFromFile(name, sep, exclude, c_attr, false);  // load un-labelled data from file
  high_resolution_clock::time_point pars2 = high_resolution_clock::now();

  high_resolution_clock::time_point knn1 = high_resolution_clock::now();
  if(opt) {
    if(dist_metr==1) {
      // Using TI-NN w.r.t. the Minkowski distance for some p
      DistCalculator d(calculateMinkowskiDistance, p);
      tinn(&dataset_unlabelled, k, d);
    }
    else if(dist_metr==0) {
      // Using TI-NN w.r.t. cosine similarity or Minkowski distance of normalized vectors
      if(transform_cosine) {
        DistCalculator d(calculateTransformedCosineToMinkowski, p, alpha);
        tinn(&dataset_unlabelled, k, d);
      }
      else {
        DistCalculator d(calculateCosineDissimilarity);
        tinn(&dataset_unlabelled, k, d);
      }
    }
    else {
      cerr<<"ERROR! The ID of the distance metric not recognized!"<<endl;
      return -1;
    }
  }
  else {
    if(dist_metr==1) {
      // Using naive NN w.r.t. the Minkowski distance for some p
        DistCalculator d(calculateMinkowskiDistance, p);
        nnn(&dataset_unlabelled, k, d, sortAsTi);
    }
    else if(dist_metr==0) {
      // Using naive NN w.r.t. the cosine simlarity (either pure cosine similarity or translated to the corresponding Euclidean (?) distance)
      if(transform_cosine) {
        DistCalculator d(calculateTransformedCosineToMinkowski, p, alpha);
        nnn(&dataset_unlabelled, k, d, sortAsTi);
      }
      else {
        DistCalculator d(calculateCosineDissimilarity);
        nnn(&dataset_unlabelled, k, d, sortAsTi);
      }
    }
    else {
      cerr<<"ERROR! The ID of the distance metric not recognized!"<<endl;
      return -1;
    }
  }
  high_resolution_clock::time_point knn2 = high_resolution_clock::now();

  high_resolution_clock::time_point nbc1 = high_resolution_clock::now();
  nbc(&dataset_unlabelled);
  high_resolution_clock::time_point nbc2 = high_resolution_clock::now();

  double time_pars = (((double)duration_cast<milliseconds>(pars2-pars1).count())/1000)/2; // Taking the mean of the time elapsed for parsing the datasset
  double time_knn = ((double)duration_cast<milliseconds>(knn2-knn1).count())/1000;
  double time_nbc = ((double)duration_cast<milliseconds>(nbc2-nbc1).count())/1000;
  double randVal = randIndex(&dataset_unlabelled, &dataset_labelled);
  double silhouette = groupSilhouetteMeasure(&dataset_unlabelled);
  double noiseShare = proportionOfNoises(&dataset_unlabelled);
  tuple<int, int, int, double> otherStats = clustStats(&dataset_unlabelled);

  vector<string> csv_headers, csv_data;
  string sect_delim = "<- INPUT PARAMS ||| OUTPUT PARAMS ->";
  csv_headers.push_back("NAME OF THE DATASET");
  csv_headers.push_back("SPECIFIED CLASS ATTRIBUTE");
  csv_headers.push_back("THE VALUE ON K USED");
  csv_headers.push_back("STRING SEPARATOR USED");
  csv_headers.push_back("COLUMNS THAT WERE EXCLUDED");
  csv_headers.push_back("OPTIMIZATION USED");
  csv_headers.push_back("ID OF THE DISTANCE METRIC APPLIED");
  csv_headers.push_back("THE VALUE ON P (ONLY RELEVANT WHEN APPLYING THE MINKOWSKI DISTANCE)");
  csv_headers.push_back("COSINE SIMILARITY TRANSFORMED TO MINKOWSKI DISTANCE (USING NORMALIZED VECTORS)");
  csv_headers.push_back("ALPHA VALUE USED (ONLY RELEVANT WHEN USING MINKOWSKI DISTANCE WITH NORMALIZED VECTORS)");
  csv_headers.push_back("OUTPUT FILE (IF SPECIFIED)");
  csv_headers.push_back(sect_delim);
  csv_headers.push_back("TIME ELAPSED FOR PARSING THE DATASET [SECONDS]");
  csv_headers.push_back("TIME ELAPSED FOR CALCULATING (REVERSED) NEAREST NEIGHBORS [SECONDS]");
  csv_headers.push_back("TIME ELAPSED FOR NORMALIZING VECTORS (IF COSINE DISSIMILARITY SELECTED AND TRANSOFRMED TO MINKOWSKI DISTANCE USING NORMALIZED VECTORS)");
  csv_headers.push_back("TIME ELAPSED FOR EXECUTING THE NBC ALGORITHM [SECONDS]");
  csv_headers.push_back("RAND INDEX BETWEEN THE CLUSTERED DATASET AND THE ORIGINAL (LABELLED) DATASET");
  csv_headers.push_back("SILHOUETTE COEFFICIENT OF THE CLUSTERED DATASET (NOISES EXCLUDED)");
  csv_headers.push_back("PROPORTION OF SAMPLES MARKED AS NOISES BY THE NBC ALGORITHM");
  csv_headers.push_back("NUMBER OF CLUSTERS USED (NOISES INCLUDED)");
  csv_headers.push_back("MAX NUMBER OF SAMPLES IN THE SAME CLUSTER (NOISES INCLUDED)");
  csv_headers.push_back("MIN NUMBER OF SAMPLES IN THE SAME CLUSTER (NOISES INCLUDED)");
  csv_headers.push_back("STANDARD DEVIATION OF CARDINALITIES (NOISES INCLUDED)");

  csv_data.push_back(name);
  csv_data.push_back(to_string(c_attr));
  csv_data.push_back(to_string(k));
  csv_data.push_back("\"" + sep + "\"");
  string excluded = "[";
  for(int i=0; i<exclude.size(); i++)
    if(i==exclude.size()-1)
      excluded += to_string(exclude.at(i));
    else
      excluded += to_string(exclude.at(i)) + ",";
  excluded += "]";
  csv_data.push_back(excluded);
  csv_data.push_back(opt ? "YES" : "NO");
  csv_data.push_back(to_string(dist_metr));
  csv_data.push_back(to_string(p));
  csv_data.push_back(transform_cosine ? "YES" : "NO");
  csv_data.push_back(to_string(alpha));
  csv_data.push_back(out_file.compare("")!=0 ? out_file : "NONE");
  csv_data.push_back(sect_delim);
  csv_data.push_back(to_string(time_pars));
  csv_data.push_back(to_string(time_knn));
  csv_data.push_back(to_string(time_norm));
  csv_data.push_back(to_string(time_nbc));
  csv_data.push_back(to_string(randVal));
  csv_data.push_back(to_string(silhouette));
  csv_data.push_back(to_string(noiseShare));
  csv_data.push_back(to_string(get<0>(otherStats)));
  csv_data.push_back(to_string(get<1>(otherStats)));
  csv_data.push_back(to_string(get<2>(otherStats)));
  csv_data.push_back(to_string(get<3>(otherStats)));

  if(csv_data.size()!=csv_headers.size()) {
    cerr<<"ERROR! The number of labels and the number of data had different sizes!"<<endl;
    return -1;
  }

  if(out_file.compare("")!=0) {
    std::streambuf *coutbuf = std::cout.rdbuf();  // save old buffer
    std::ofstream out(out_file);
    if (out_file.length() > 0) {
      std::cout.rdbuf(out.rdbuf());  // change buffer
    }

    if(headers)
      for(int i=0; i<csv_headers.size(); i++)
        if(i==csv_headers.size()-1)
          cout<<csv_headers.at(i) + '\n';
        else
          cout<<csv_headers.at(i) + ",";

    for(int i=0; i<csv_data.size(); i++)
      if(i==csv_data.size()-1)
        cout<<csv_data.at(i) + '\n';
      else
        cout<<csv_data.at(i) + ",";

    cout.flush();
    if (out_file.length() > 0) {
      std::cout.rdbuf(coutbuf);  // restore old buffer
    }
  }

  cout<<"**********************************RESULTS**********************************"<<endl;
  cout<<"------------------------------------INPUT----------------------------------"<<endl;

  int old_i = 0;
  for(int i=0; i<csv_data.size() && csv_data.at(i).compare(sect_delim)!=0; i++) {
    cout<<csv_headers.at(i) + ": " + csv_data.at(i)<<endl;
    old_i = i;
  }
  cout<<endl;

  cout<<"-----------------------------------OUTPUT----------------------------------"<<endl;

  for(int i=old_i+2; i<csv_data.size(); i++)
    cout<<csv_headers.at(i) + ": " + csv_data.at(i)<<endl;

  cout<<"*****************************************************************************"<<endl;

  return 0;
}
