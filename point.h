#pragma once
#include <vector>

using namespace std;

/*
 * author: Łukasz Patro
 */
class Point {
public:
  Point(vector<double> *data);

private:
  // unsigned int id;
  int clusterNumber = -1;
  double ndf = -1;
  vector<Point *> kpNN = vector<Point *>();
  unsigned int rkpNNCount = 0;
  vector<double> data;

public:
  int getClusterNumber();
  void setClusterNumber(int clusterNumber);
  double getNdf();
  void setNdf(double ndf);
  vector<Point *> getKpNN();
  void addKpNN(Point *point);
  unsigned int getRkpNNCount();
  void incrementRkpNNCount();
  vector<double> *getData();
};
