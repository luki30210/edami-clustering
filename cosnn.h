#pragma once
#include <math.h>
#include <vector>
#include "point.h"
#include "cosine_metric.h"
#include "naivenn.h"
using namespace std;

/*
 * Returns calculated cosine similarity between points
 * author: Łukasz Patro (Modified by Sylwester Liljegren)
 */
double calculateCosineSimilarity(Point* p1, Point* p2) {
  return cosSimilarity(p1->getData()->begin(), p1->getData()->end(), p2->getData()->begin(), p2->getData()->end());
}

/*
 * Returns calculated similarity between points.
 * Prepeared if there would be more then one measure of distance.
 * author: Łukasz Patro (Modified by Sylwester Liljegren)
 */
double calculateSimilarity(Point* p1, Point* p2) {
  return calculateCosineSimilarity(p1, p2);
}

/*
 * Distance comparator for DistancePoint class.
 * author: Łukasz Patro
 */
bool compSimilarityPoints(const DistancePoint* lhs, const DistancePoint* rhs) {
  return lhs->distance > rhs->distance;
}


/*
 * Method for calculating k+ nearest neighbours and reverse k+ nearest neighbours using cosine similarity.
 * author: Łukasz Patro (Modified by Sylwester Liljegren)
 */
void cosnn(vector<Point>* dataset, int k) {
  for (int p = 0; p < dataset->size(); p++) {
    vector<DistancePoint*> distances = vector<DistancePoint*>();
    for (int rp = 0; rp < dataset->size(); rp++) {
      if(p!=rp) {
        DistancePoint* distancePoint = new DistancePoint(&dataset->at(rp), calculateSimilarity(&dataset->at(p), &dataset->at(rp)));
        distances.push_back(distancePoint);
      }
    }

    sort(begin(distances), end(distances), compSimilarityPoints);

    int tempK = k;
    while (distances.at(tempK) == distances.at(tempK + 1)) {    // first element of distances always with distance = 0
      tempK++;
    }

    for (int i = 1; i <= tempK; i++) {
      dataset->at(p).addKpNN(distances.at(i)->refPoint);
      distances.at(i)->refPoint->incrementRkpNNCount();
    }
  }
}
