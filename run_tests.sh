#!/bin/bash

echo "3.2.1_cnae.csv params: -c 1"
./build/main -i datasets/CNAE-9.data -c 1 -f outFiles/3.2.1_cnae.csv

echo "3.2.2_cnae.csv params: c 1 -d 0"
./build/main -i datasets/CNAE-9.data -c 1 -d 0 -f outFiles/3.2.2_cnae.csv

echo "3.2.3_cnae.csv params: -c 1 -d 0 -t"
./build/main -i datasets/CNAE-9.data -c 1 -d 0 -t -f outFiles/3.2.3_cnae.csv

echo "3.3.1_cnae.csv params: -c 1 -o"
./build/main -i datasets/CNAE-9.data -c 1 -o -f outFiles/3.3.1_cnae.csv

echo "3.3.2_cnae.csv params: -c 1 -d 0 -o"
./build/main -i datasets/CNAE-9.data -c 1 -d 0 -o -f outFiles/3.3.2_cnae.csv

echo "3.3.3_cnae.csv params: -c 1 -d 0 -o -t"
./build/main -i datasets/CNAE-9.data -c 1 -d 0 -o -t -f outFiles/3.3.3_cnae.csv

echo "3.3.4_cnae.csv params: -c 1 -d 0 -a 1000 -o -t"
./build/main -i datasets/CNAE-9.data -c 1 -d 0 -a 1000 -o -t -f outFiles/3.3.4_cnae.csv

echo "3.3.5_cnae.csv params: -c 1 -k 10 -o where k=log2(1081)=~10"
./build/main -i datasets/CNAE-9.data -c 1 -k 10 -o -f outFiles/3.3.5_cnae.csv


echo "3.2.1_epi.csv params: -e 1 -c 179"
./build/main -i datasets/epileptic.data -e 1 -c 179 -f outFiles/3.2.1_epi.csv

echo "3.2.2_epi.csv params: -e 1 -c 179 -d 0"
./build/main -i datasets/epileptic.data -e 1 -c 179 -d 0 -f outFiles/3.2.2_epi.csv

echo "3.2.3_epi.csv params: -e 1 -c 179 -d 0 -t"
./build/main -i datasets/epileptic.data -e 1 -c 179 -d 0 -t -f outFiles/3.2.3_epi.csv

echo "3.3.1_epi.csv params: -e 1 -c 179 -o"
./build/main -i datasets/epileptic.data -e 1 -c 179 -o -f outFiles/3.3.1_epi.csv

echo "3.3.2_epi.csv params: -e 1 -c 179 -d 0 -o"
./build/main -i datasets/epileptic.data -e 1 -c 179 -d 0 -o -f outFiles/3.3.2_epi.csv

echo "3.3.3_epi.csv params: -e 1 -c 179 -d 0 -o -t"
./build/main -i datasets/epileptic.data -e 1 -c 179 -d 0 -o -t -f outFiles/3.3.3_epi.csv

echo "3.3.4_epi.csv params: -e 1 -c 179 -d 0 -a 1000 -o -t"
./build/main -i datasets/epileptic.data -e 1 -c 179 -d 0 -a 1000 -o -t -f outFiles/3.3.4_epi.csv

echo "3.3.5_epi.csv params: -e 1 -c 179 -k 13 -o where k=log2(11501)=~13"
./build/main -i datasets/epileptic.data -e 1 -c 179 -k 13 -o -f outFiles/3.3.5_epi.csv



echo "3.2.1_mov.csv params: -c 91"
./build/main -i datasets/movement_libras.data -c 91 -f outFiles/3.2.1_mov.csv

echo "3.2.2_mov.csv params: -c 91 -d 0"
./build/main -i datasets/movement_libras.data -c 91 -d 0 -f outFiles/3.2.2_mov.csv

echo "3.2.3_mov.csv params: -c 91 -d 0 -t"
./build/main -i datasets/movement_libras.data -c 91 -d 0 -t -f outFiles/3.2.3_mov.csv

echo "3.3.1_mov.csv params: -c 91 -o"
./build/main -i datasets/movement_libras.data -c 91 -o -f outFiles/3.3.1_mov.csv

echo "3.3.2_mov.csv params: -c 91 -d 0 -o"
./build/main -i datasets/movement_libras.data -c 91 -d 0 -o -f outFiles/3.3.2_mov.csv

echo "3.3.3_mov.csv params: -c 91 -d 0 -o -t"
./build/main -i datasets/movement_libras.data -c 91 -d 0 -o -t -f outFiles/3.3.3_mov.csv

echo "3.3.4_mov.csv params: -c 91 -d 0 -a 1000 -o -t"
./build/main -i datasets/movement_libras.data -c 91 -d 0 -a 1000 -o -t -f outFiles/3.3.4_mov.csv

echo "3.3.5_mov.csv params: -c 91 -k 8 -o where k=log2(360)=~8"
./build/main -i datasets/movement_libras.data -c 91 -k 8 -o -f outFiles/3.3.5_mov.csv
