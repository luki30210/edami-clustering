#pragma once
#include "point.h"

using namespace std;

/*
 * Helper class to contain reference to second point and distance between this point and selected one.
 * author: Łukasz Patro
 */
class DistancePoint {
public:
  DistancePoint(Point* refPoint, double distance) {
    this->refPoint = refPoint;
    this->distance = distance;
  }
  Point* refPoint;
  double distance;
};

/*
 * Distance comparator for DistancePoint class.
 * author: Łukasz Patro
 */
bool compDistancePoints(const DistancePoint* lhs, const DistancePoint* rhs) {
  return lhs->distance < rhs->distance;
}
