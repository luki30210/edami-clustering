#pragma once
#include "DistCalculator.h"
#include "distance_point.h"
#include "point.h"
#include "tinn_base.h"
#include <algorithm>
#include <math.h>
#include <vector>

using namespace std;

/*
 * Naive version for calculating k+ nearest neighbours and reverse k+ nearest neighbours.
 * author: Łukasz Patro
 */
void nnn(vector<Point>* dataset, int k, DistCalculator dist, bool sortDatasetAccordingToReferencePoint) {
  if (sortDatasetAccordingToReferencePoint) {
    vector<Point *> datasetSortedRef = getDataSetSorted(dataset, dist);
    for (int i = 0; i < datasetSortedRef.size(); i++) {
      dataset->at(i) = *datasetSortedRef.at(i);
    }
  }

  for (int p = 0; p < dataset->size(); p++) {
    vector<DistancePoint *> distances = vector<DistancePoint *>();
    for (int rp = 0; rp < dataset->size(); rp++) {
      DistancePoint *distancePoint = new DistancePoint(&dataset->at(rp), dist(&dataset->at(p), &dataset->at(rp)));
      distances.push_back(distancePoint);
    }

    sort(begin(distances), end(distances), compDistancePoints);

    int tempK = k;
    while (distances.at(tempK) == distances.at(tempK + 1)) {    // first element of distances always with distance = 0
      tempK++;
    }

    for (int i = 1; i < tempK; i++) {
      dataset->at(p).addKpNN(distances.at(i)->refPoint);
      distances.at(i)->refPoint->incrementRkpNNCount();
    }
  }
}
