#pragma once

#include "point.h"

/*
 * author: Łukasz Patro
 */
void calculateNdf(vector<Point> *dataset) {
    for (int p = 0; p < dataset->size(); p++) {
        double ndf = (double) dataset->at(p).getRkpNNCount() / (double) dataset->at(p).getKpNN().size();
        dataset->at(p).setNdf(ndf);
    }
}

/*
 * author: Łukasz Patro
 */
void nbc(vector<Point> *dataset) {
    calculateNdf(dataset);  // calculate ndf for each point (k+NN list and Rk+nn count required!)

    int clusterCount = 0;

    // for each point p in dataset
    for (int p = 0; p < dataset->size(); p++) {

        // if p is clustered or is not DP -> skip
        if (dataset->at(p).getClusterNumber() != -1 || dataset->at(p).getNdf() < 1) {
            continue;
        }

        // if p is a DP or EP, hen create a new cluster, denoted as p’s cluster
        dataset->at(p).setClusterNumber(clusterCount);

        // initialize dpSet
        vector<Point *> dpSet = vector<Point *>();

        // for each point q in nearest neighbours of p
        for (int q = 0; q < dataset->at(p).getKpNN().size(); q++) {
            // add to the cluster
            dataset->at(p).getKpNN().at(q)->setClusterNumber(clusterCount);
            // if point q is DP -> add to dpSet
            if (dataset->at(p).getKpNN().at(q)->getNdf() >= 1) {
                dpSet.push_back(dataset->at(p).getKpNN().at(q));
            }
        }

        // while dpSet is not empty -> expanding the cluster
        while (dpSet.size() > 0) {
            // get first point from dpSet
            Point * point = dpSet.at(0);

            // iterate for all point's nearest neighbours
            for (int q = 0; q < point->getKpNN().size(); q++) {
                // if q is already clustered -> skip
                if (point->getKpNN().at(q)->getClusterNumber() != -1) {
                    continue;
                }

                // add to the cluster
                point->getKpNN().at(q)->setClusterNumber(clusterCount);

                // if point q is DP -> add to dpSet
                if (point->getKpNN().at(q)->getNdf() >= 1) {
                    dpSet.push_back(point->getKpNN().at(q));
                }
            }

            dpSet.erase(dpSet.begin());
        }

        clusterCount++;
    }
}
