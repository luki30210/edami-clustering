#pragma once
#include <iostream>
#include <cmath>
#include <stdexcept>
using namespace std;

template<class T>
double cosSimilarity(T begin1, T end1, T begin2, T end2) {
  if(end1-begin1!=end2-begin2)
    throw runtime_error("ERROR: The two data structures had different numbers of elements!!!!!");

  double numerator = 0;
  double denom1 = 0;
  double denom2 = 0;

  while(begin1<end1 && begin2<end2) {
    numerator += (*begin1)*(*begin2);
    denom1 += (*begin1)*(*begin1);
    denom2 += (*begin2)*(*begin2);

    begin1++;
    begin2++;
  }

  double denominator = sqrt(denom1)*sqrt(denom2);
  return numerator/denominator;
}
