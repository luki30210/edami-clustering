#pragma once
#include "cosine_metric.h"
#include "point.h"
#include <chrono>
#include <iostream>
#include <vector>

using namespace std;
using namespace std::chrono;

extern double time_norm = 0; // Store the total amount of time spent on normalizing vectors

/*
 * Returns calculated Minkowski distance between points
 * author: Łukasz Patro
 */
double calculateMinkowskiDistance(Point* p1, Point* p2, int p = 2, double alpha = 1) {
  double result = 0;
  for (int d = 0; d < p1->getData()->size(); d++) {
    double component = p1->getData()->at(d) - p2->getData()->at(d);
    result += pow(component < 0 ? -component : component, (double)p);
  }
  return pow(result, 1/(double)p);
}

double calculateCosineDissimilarity(Point* p1, Point* p2, int p = 2, double alpha = 1) {
  return 1-cosSimilarity(p1->getData()->begin(), p1->getData()->end(), p2->getData()->begin(), p2->getData()->end());
}

double calculateTransformedCosineToMinkowski(Point* p1, Point* p2, int p = 2, double alpha = 1) {
  vector<double> comps1 = *(p1->getData());
  vector<double> comps2 = *(p2->getData());
  if(comps1.size()!=comps2.size())
    throw runtime_error("ERROR: The two data structures had different numbers of elements!!!!!");

  double sum1 = accumulate(comps1.begin(), comps1.end(), 0);
  double sum2 = accumulate(comps2.begin(), comps2.end(), 0);

  high_resolution_clock::time_point nv1 = high_resolution_clock::now();
  Point* NFp1 = new Point(&comps1);
  Point* NFp2 = new Point(&comps2);
  for(int i=0; i<comps1.size(); i++) {
    NFp1->getData()->at(i) *= (alpha/sum1);
    NFp2->getData()->at(i) *= (alpha/sum2);
  }
  high_resolution_clock::time_point nv2 = high_resolution_clock::now();
  time_norm += ((double)duration_cast<milliseconds>(nv2-nv1).count())/1000;

  double res = calculateMinkowskiDistance(NFp1, NFp2, p);
  delete NFp1;
  delete NFp2;

  return res;
}

class DistCalculator {
private:
  int p = 2;
  double alpha = 1;
  double (*d)(Point*, Point*, int, double);
public:
  DistCalculator(double (*dist)(Point*, Point*, int, double), int mink_p, double a) : d(dist), p(mink_p), alpha(a) {}
  DistCalculator(double (*dist)(Point*, Point*, int, double), int mink_p) : d(dist), p(mink_p) {}
  DistCalculator(double (*dist)(Point*, Point*, int, double)) : d(dist) {}

  double operator()(Point* p1, Point* p2) {
    return (*d)(p1, p2, p, alpha);
  }
};
